<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'IndexController@home');


Route::get('/register', 'AuthController@bio');

Route::post('/welcome', 'AuthController@kirim');


Route::get('/data-table', function(){
    return view('table.data-table');
});


//CRUD Kategori
//Create
Route::get('/cast/create', 'CastController@create');

//Store
Route::post('/cast', 'CastController@store');

//Read
Route::get('/cast', 'CastController@index');

//Read by Id
Route::get('/cast/{cast_id}', 'CastController@show');

//Edit
Route::get('/cast/{cast_id}/edit', 'CastController@edit');

//Update
Route::put('/cast/{cast_id}', 'CastController@update');

//Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy');
