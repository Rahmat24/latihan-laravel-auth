@extends('layout.master')

@section('judul')
Halaman Edit
@endsection

@section('content')
<form action="/cast/{{$Cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" name="nama" value="{{$Cast->nama}}" placeholder="Masukkan Nama">
        @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Umur</label>
        <input type="int" class="form-control" name="umur" value="{{$Cast->umur}}" placeholder="Masukkan Umur">
        @error('umur')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control" placeholder="Masukkan Bio">{{$Cast->bio}}</textarea>
        @error('bio')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection