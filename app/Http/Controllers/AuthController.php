<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view('halaman.register');
    }

    public function kirim(request $request){       
        $FirstName=$request['FirstName'];
        $LastName=$request['LastName'];
        return view('halaman.welcome', compact ('FirstName','LastName'));
    }
}
